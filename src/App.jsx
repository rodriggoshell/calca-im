import { useState } from 'react';
import './App.css';

function App() {
  const [peso, setPeso] = useState('');
  const [altura, setAltura] = useState('');
  const [message, setMessage] = useState('');

  function calcu(e){
    e.preventDefault();
    const alt = altura / 100;
    const imc = peso / (alt * alt);
    alert(imc.toFixed(2));

    if(imc < 18.6) {
      setMessage('Voce esta abaixo do peso! Seu IMC: ' + imc.toFixed(2));
    }
    else if(imc >= 18.6 && imc < 24.9){
      setMessage('Peso ideal! Seu IMC: ' + imc.toFixed(2));
    }
    else if(imc >= 24.9 && imc <= 34.9) {
      setMessage('Voce esta levemente a cima do peso! Seu IMC: ' + imc. toFixed(2));
    }
    else if(imc > 34.9) {
      setMessage('Alerta!Seu IMC:' + imc.toFixed(2));
    }

  }

  return (
    <div className="app">
      <h1>Calculadora de IMC</h1>
      <span>Vamos calcular seu imc</span>
      <div className="area-input">
        <form>
          <input type="text" vlue={peso}
            onChange={(e)=>setPeso(e.target.value)}
            placeholder="peso em (kg) Ex:90"
          />
          <input type="text" vlaue={altura} 
            onChange={(e)=>setAltura(e.target.value)}
            placeholder="Aaltura em (cm) Ex: 1.80"
          />
          <button onClick={calcu}>calcular</button>
        </form>
      </div>
      <h2>{message}</h2>
    </div>
  )
}

export default App
